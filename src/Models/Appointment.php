<?php 
namespace Sunnydevbox\TWAppointment\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class Appointment extends BaseModel
{
	protected $fillable = [
		'event_id',
		'appointee_id',
	];

	public function getTable()
	{
		return config('tw-appointment.tables.appointment');
	}
}