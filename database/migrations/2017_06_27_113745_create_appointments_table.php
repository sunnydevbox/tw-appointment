<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Get users model
        $userModel = config('auth.providers.'.config('auth.guards.'.config('auth.defaults.guard').'.provider').'.model');
        
        Schema::create(config('tw-appointment.tables.appointments'), function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            
            // TYPE OF ENTITY THAT IS BOOKABLE
            // ex: - events for appointment
            //     - rooms
            //     - items
            $table->integer('event_id')->unsigned();
            $table->integer('appointee_id')->unsigned();

            $table->text('notes');


            $table->index('event_id');
            $table->index('appointee_id');

            $table->foreign('event_id')
                ->references('id')
                ->on(config('tw-events.tables.events'))
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('agent_id')->references('id')->on((new $userModel())->getTable())
                  ->onDelete('cascade')->onUpdate('cascade');


            $table->softDeletes();
            $table->timestamps();


            // Columns
            // $table->increments('id');
            // $table->morphs('bookable');
            // $table->integer('customer_id')->unsigned();
            // $table->integer('agent_id')->unsigned();
            // $table->timestamp('starts_at')->nullable();
            // $table->timestamp('ends_at')->nullable();
            // $table->decimal('price')->default('0.00');
            // $table->{$this->jsonable()}('price_equation')->nullable();
            // $table->timestamp('cancelled_at')->nullable();
            // $table->text('notes')->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('tw-appointment.tables.appointments'), function (Blueprint $table) {
            $table->dropForeign(config('tw-appointment.tables.appointments') . '_event_id_foreign');
            $table->dropForeign(config('tw-appointment.tables.appointments') . '_appointee_id_foreign');
            $table->dropIndex(config('tw-appointment.tables.appointments') . '_event_id_index');
            $table->dropIndex(config('tw-appointment.tables.appointments') . '_appointee_id_index');
        });

        Schema::dropIfExists(config('tw-appointment.tables.appointments'));
    }

    /**
     * Get jsonable column data type.
     *
     * @return string
     */
    protected function jsonable()
    {
        return DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql'
               && version_compare(DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge')
            ? 'json' : 'text';
    }
}
