<?php


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {


	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) {
		
		$api->resource('appointments', 			'Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\UserEventController');

	});
}