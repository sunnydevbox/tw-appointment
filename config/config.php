<?php

return [
	'tables' => [
		'appointments' => 'appointments',
	],

	'models' => [
		'appointment' => Sunnydevbox\TWAppointment\Models\Appointment::class,
	],

	'repositories' => [
		'appointment'	=> Sunnydevbox\TWAppointment\Repositories\Appointment\AppointmentRepository::class,
	],

	'controllers' => [
		'appointment' 	=> Sunnydevbox\TWAppointment\Htp\Controllers\API\V1\AppointmentController::class, 
	],
];